# pool.sh a simple processes pool management

**What is pool.sh ?** pool.sh allows you to create simple pools of processes. This is usefull to run several process in parallel such as simulations or computations. It is designed to be used in bash scripts.

**How it works ?**
- Create a pool with `./pool.sh create <poolname> <capacity>`
- Launch a process in a pool with `./pool.sh run <poolname> <command>` which return the process id `<procid>`. If the pool `<poolname>` is full (number of processes equals to `<capacity>`) pool.sh will wait until room has been made in the pool to run `<command>`.
- Get the output of a process with `./pool.sh cat <poolname> <procid>`
- Wait until all the processes are completed with `./pool.sh wait <poolname>` 
- Remove a pool with `./pool.sh remove <poolname>`. If processes are still running in the `<poolname>` this call will fail.
- Force the removal of a pool with `./pool.sh remove-force <poolname>`. All running processes will be killed and the pool will be deleted
- Additional properties can be attached to processes with `./pool.sh setp <poolname> <procid> <propname> <propvalue>`. Default properties are `<cmd>`,`<procpid>`,`<startat>`.
- Properties can be retrieve with `./pool.sh getp <poolname> <procid> <propname>`
- Processes in a pool can be paused with `./pool.sh pause <poolname>`
- Processes in a pool can be resumed with `./pool.sh resume <poolname>`
- Available pools can be retrieve with `./pool.sh ls`
- The status of a pool can be seen with `./pool.sh status <poolname>`
- All the processes output file can be retrieve with `./pool.sh ls-output <poolname>`

**Final quote:** No checks are performed the arguments. Thus, be sure to use the right ones. The default configuration requires access to the `/tmp` directory and uses name conventions which may clash with the existing content of `/tmp` if you are very unlucky. If in this the case, you can edit `pool.sh` to avoid those clashes.
